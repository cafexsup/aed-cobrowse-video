


This repo demonstrates using AED with FCSDK and Live Assist to initiate calls and cobrowse sessions from the agent side, given both parties share a known correlation id via some other channel.

-          Customer page generates CorrelatioID (url param ?cid=abc123)
-          Customer page gets session token
-          Customer page subscribes to AED(2), using CorrelationID
-          Agent receives CorrelationID via chat channel (details not important here, just assume that agent has CorrelationID)
-          Agent page gets session token
-          Agent page subscribes to AED(2), using CorrelationID
-          Agent page publish a message to AED (‘cobrowse’ or ‘videocall’ + ‘agent extension number’)
-          Customer page receives message from AED
-          Customer page initiate either cobrowse or video call (depending on command that has been sent)
