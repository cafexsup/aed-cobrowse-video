
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://rmorgan-latest.cafex.com:8443/assistserver/sdk/web/agent/css/assist-console.css">
    <link rel="stylesheet" href="https://rmorgan-latest.cafex.com:8443/assistserver/sdk/web/shared/css/shared-window.css">
    
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
    
    
    
    <!-- Set the style of the call quality indicator -->
    <style type="text/css">

        #quality {
            height: 25px;
            width: 25px;
            background-repeat: no-repeat;
            background-size: cover;
            top: 10px;
            left: 25px;
            position: absolute;
            border-radius: 50%;
            opacity:0.5;
        }

        #quality.call-quality-good {
            background-color: green;
        }

        #quality.call-quality-moderate {
            background-color: yellow;
        }

        #quality.call-quality-poor {
            background-color: red;
        }

        #quality.no-call {
            display: none;
        }

        #local {
            width: 120px;
            height: 90px;
            position:absolute;
            top:140px;
            right:25px;
            z-index: 1000;
        }

        #remote {
            width: 350px;
            height: 240px;
        }
        
        .remote-view-element {
            /*max-width:100%;*/
        }

        #share-container {
            /*width: 800px;
            height: 600px;*/
        }
        
        #shareview {
            border: 2px solid #888;
            border-radius: 4px;
            overflow: hidden;
            /*position: absolute;*/
            background-color: #f9f9f9;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            min-width: 100%;
            min-height: 100%;
        }

        #local, #remote {
            
            border: 1px solid grey;
        }

        .shared-window.active {
            pointer-events: all;
        }
        .interactive.active {
            cursor: pointer;
        }
        .shared-window {
            pointer-events: none;
        }
        .child-shared-div {
            position: absolute;
            width: 0px;
            height: 0px;
        }

    </style>
</head>
<body>  

<div class='container' >
<h1 class='text-center'>Agent Assist</h1>
<hr/>
<div class='col-md-8' >
    <!-- remote screen share -->
    
        
    <div id="share-container" class="" style="position: absolute; width: 720px; height: 480px; overflow: hidden;">
        
    <div id="shareview" class=''></div>

    </div>

</div>  
<div class='col-md-4' >
    <!-- remote video view -->
    <div id="remote" class='well'>
    </div>
    <div id="local" ></div>
    <!-- local video preview -->


    <!-- indicates the quality of the call -->
    <div id="quality"></div>

    <button id="call" class='btn btn-success'><i class='glyphicon glyphicon-share'></i> Video Call</button>

    <button id="cobrowse" class='btn btn-info'><i class='glyphicon glyphicon-share'></i> Cobrowse</button>

    <button id="request-share" class='btn btn-primary hidden'><i class='glyphicon glyphicon-share'></i> Screen share</button>

    <button id="hangup" class='btn btn-danger'><i class='glyphicon glyphicon-phone-alt'></i> End </button>
<br/><br/>
<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#mode" aria-controls="home" role="tab" data-toggle="tab">Mode</a></li>
    <li role="presentation"><a href="#push" aria-controls="profile" role="tab" data-toggle="tab">Push</a></li>  
    <li role="presentation"><a href="#annotation" aria-controls="profile" role="tab" data-toggle="tab">Annotation</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="mode">
    <br/>
        <div class="panel panel-default">
    <div class="panel-heading">Mode</div>
        <div class="panel-body">
    <form class="mode">
        <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-default">
                    <input type="radio" name="mode" value="control" /><i class='glyphicon glyphicon-hand-up'></i> Grab<br>
                </label>
         <label class="btn btn-default">
        <input type="radio" name="mode" value="draw" /><i class='glyphicon glyphicon-pencil'></i> Draw<br>
        </label>
         <label class="btn btn-default">
        <input type="radio" name="mode" value="spotlight" /><i class='glyphicon glyphicon-record'></i> Pointer<br>
        </label>
        
    </div>
    
    </form>
    
    <br/>
    <button class="btn btn-default pull-left" type="submit" id='clear'><i class='glyphicon glyphicon-trash'></i> Clear</button>
    
    </div>
    </div>
    

    
    
    </div>
       <div role="tabpanel" class="tab-pane" id="annotation">
       <br/>
        <div class="panel panel-default">
    <div class="panel-heading">Annotation Controls</div>
        <div class="panel-body">
                <form class="annotation-control">
                    <label>Colour</label>
                    <input type="color" class="color" value="#ff0000">
                    <br/><br/>

                    <label>Opacity</label>
                    <select class="form-control" type="text" class="opacity" value="0.5">
                          <option>0.2</option>
                          <option>0.4</option>
                          <option>0.6</option>
                          <option>0.8</option>
                          <option>1</option>
                        </select>
                        <br>
                        <label>Thickness</label>
                        <select class="form-control" type="text" class="width" value="0.5">
                          <option>1</option>
                          <option>2</option>
                          <option>4</option>
                          <option>8</option>
                          <option>10</option>
                        </select>
                        <br/>
                    <button type="submit" class='btn btn-primary btn-sm pull-right'>Update</button>
                </form>
            </div>
        </div>

</div>
    
    
    <div role="tabpanel" class="tab-pane" id="push">
        <br/>
            <div class="panel panel-default">
                <div class="panel-heading">Push Options</div>
                    <div class="panel-body">
                <form class="push form-inline">
                    <div class="input-group">
                 <div class="input-group-addon">http://</div>
                    <input type="text" class="url form-control" placeholder="URL"><br>
                    </div><br><br/>
                    <input type="radio" name="type" value="document" class='document'> Document<br>
                    <input type="radio" name="type" value="content"> Content<br>
                    <input type="radio" name="type" value="link"> Link<br>
                    <button type="submit" class='btn btn-primary btn-sm pull-right' >Push</button>
                </form>
                
                </div>
                </div>
    
    </div>
  </div>

</div>
        

</div>
</div>

</body>
<script src="https://rmorgan-latest.cafex.com:8443/assistserver/sdk/web/shared/js/thirdparty/i18next-1.7.4.min.js"></script>

<!-- libraries needed for voice / video -->
<script src="https://rmorgan-latest.cafex.com:8443/gateway/adapter.js"></script>
    <script src="https://rmorgan-latest.cafex.com:8443/gateway/csdk-sdk.js"></script>

<!-- other libraries -->
<script src="https://rmorgan-latest.cafex.com:8443/assistserver/sdk/web/shared/js/assist-aed.js"></script>
<script src="https://rmorgan-latest.cafex.com:8443/assistserver/sdk/web/shared/js/shared-windows.js"></script>
<script src="https://rmorgan-latest.cafex.com:8443/assistserver/sdk/web/agent/js/assist-console.js"></script>

<!-- needed for voice / video -->

<!-- load jQuery - helpful for DOM manipulation -->
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>

<!-- control -->
<script>
    $(function () {
    
        $('#myTabs a').click(function (e) {
          e.preventDefault()
          $(this).tab('show')
        });

        $('#hangup').click(function(){
            AssistAgentSDK.endSupport();  
        });
        
        $('#clear').click(function(){
            AssistAgentSDK.clearSelected(); 
        });
        
        $(".mode input").click(function() {
            $(this).attr({'checked': 'checked'}).toggle();
                console.log('this');
        });

        // set the remote screen share view
        var share = $('#shareview')[0];
        AssistAgentSDK.setRemoteView(share);

        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
            function(m,key,value) {
              vars[key] = value;
            });
            return vars;
          }

          window.cid = getUrlVars()["cid"];

          UC.onInitialised = function() {

            console.log("Initialised UC");

            
             //create a topic
            var topic = UC.aed.createTopic(window.cid);
             //callback for when Topic is connected


            $("#call").on("click", function() {
                topic.submitData("videocall", "sip:1001@rmorgan-latest.cafex.com");



            });

            $("#cobrowse").on("click", function() {
                topic.submitData("cobrowse", "true");

                AssistAgentSDK.startSupport({sessionToken: window.sessionToken, url: "https://rmorgan-latest.cafex.com:8443", correlationId: window.cid});
               AssistAgentSDK.requestScreenShare();

            });


            topic.connect();

          };

                        
        $.post('session.php?cid='+ window.cid, function (sessionID) {
            window.sessionToken = sessionID;
            
            //UC init subscription

            UC.start(window.sessionToken);
                    
        });


        /////////////////////////////////////
        //
        // Configure UI handlers

        // bind to configure the screen
        $('#request-share').click(function (event) {
            event.preventDefault();
            AssistAgentSDK.requestScreenShare();
        });

        // configure the annotation
        $('.annotation-control').submit(function (event) {
            // prevent normal form submission behaviour
            event.preventDefault();

            // extract the vars
            var $this = $(this);
            var color = $this.find('.color').val();
            var opacity = $this.find('.opacity').val();
            var width = $this.find('.width').val();

            // update the annotation draw style
            AssistAgentSDK.setAgentDrawStyle(color, opacity, width);
        });

        // configure document push
        $('.push').submit(function (event) {
            // prevent the normal form submission behaviour
            event.preventDefault();

            var $this = $(this);
            var url = $this.find('.url').val();
            var type = $this.find('[name=type]:checked').val();

            var method, callback;
            switch(type) {
                case 'content':
                    AssistAgentSDK.pushContent('https://rmorgan-latest.cafex.com:8443/assist-resourcemanager/rest/public/resources/gecko.jpg');
                    break;
                case 'document':
                    AssistAgentSDK.pushDocument('https://rmorgan-latest.cafex.com:8443/assist-resourcemanager/rest/public/resources/ipad-user-guide.pdf');
                    break;
                case 'link':
                    AssistAgentSDK.pushLink('http://'+url);
                    break;
            }

            
        });

        // respond to changes in the share type
        $('[name=mode]').change(function () {
            var $this = $(this);
            var mode = $this.val();
            
            // determine which mode to switch in to
            switch(mode) {
                case 'control': 
                    AssistAgentSDK.controlSelected();
                    break;
                case 'draw':
                    AssistAgentSDK.drawSelected();
                    break;
                case 'spotlight':
                    AssistAgentSDK.spotlightSelected();
                    break;
            }
        });

        //////////////////////////////////////////
        //
        // set the AssistAgentSDK callbacks

        AssistAgentSDK.setCallEndedCallback(function () {

        });

        AssistAgentSDK.setConnectionEstablishedCallback(function () {

        });

        AssistAgentSDK.setConnectionLostCallback(function () {

        });

        AssistAgentSDK.setConnectionReestablishedCallback(function () {

        });

        AssistAgentSDK.setConnectionRetryCallback(function () {

        });

        AssistAgentSDK.setConsumerJoinedCallback(function () {
            console.log("alert: setConsumerJoinedCallback");
        });

        AssistAgentSDK.setConsumerLeftCallback(function () {
            console.log("alert: setConsumerLeftCallback");
        });

        AssistAgentSDK.setFormCallBack(function (form) {


        });

        AssistAgentSDK.setRemoteViewCallBack(function (x, y) {
            console.log('x: ' + x + ', y: ' + y);

        });

        AssistAgentSDK.setScreenShareActiveCallback(function(active) {
            console.log('Screenshare active: ' + active);

        });

        AssistAgentSDK.setScreenShareRejectedCallback(function () {

        });

        AssistAgentSDK.setSnapshotCallBack(function (snapshot) {
            // open the snapshot image
            window.open(snapshot);
        }); 


        ////////////////////////////////////
        //
        // Configure the CallManager

        // extract the elems
        var remote = $('#remote')[0];
        var local = $('#local')[0];
        var quality = $('#quality')[0];

    });
</script>
</html>
