<?php
    header("Access-Control-Allow-Origin: *");
    // configure the JSON to use in the session.

   $json = '{
            "webAppId": "webapp-id-example",
            "allowedOrigins": [],
            "urlSchemeDetails": {
                "secure": "true",
                "host": "rmorgan-latest.cafex.com",
                "port": "8443"
            },
            "voice": {
                "username": "agent1",
                "password": "",
                 "domain": "rmorgan-latest.cafex.com"
             },

             "aed":
             {
                   "accessibleSessionIdRegex":".*",
                   "maxMessageAndUploadSize":"5000",
                   "dataAllowance":"5000"
            },  
            
                "AED2.allowedTopic":"%s"
            }
            
        }';

        $cid = $_GET['cid'];

        $json = sprintf($json, $cid);

	 // configure the curl options
    $ch = curl_init("http://rmorgan-latest.cafex.com:8080/gateway/sessions/session");
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(         
        'Content-Type: application/json',
        'Content-Length: ' . strlen($json)
    ));

    // execute HTTP POST & close the connection
    $response = curl_exec($ch);
	
	if(curl_errno($ch))
		{
			echo 'error:' . curl_error($ch);
		}

     curl_close($ch);

    // decode the JSON and pick out the session token
    $decodedJson = json_decode($response);
    $id = $decodedJson->{'sessionid'};
   

    // echo the ID we've retrieved
    echo $id; 

?>